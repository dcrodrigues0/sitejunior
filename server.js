//CARREGA E CONFIGURA EXPRESS
const app = require('./src/config/custom-express');

const requireDir = require('requiredir');

//CRIA NOSSA DATABASE
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/cabelereiro', 
    {useNewUrlParser: true}
);
requireDir('./src/models');

// Rotas
app.use('/api', require('./src/routes'));

const porta = 3000;
app.listen(porta, () => {
    console.log('Servidor Está Rodando Na Porta: ' + porta);
});

