const express = require("express");
const routes = express.Router();

const mongoose = require("mongoose");

const ClientController = require('./controllers/ClientController');

routes.get('/home', (req,res) =>{
    res.send('<h3>hello world</h3>');
});

routes.get('/adminPanel', (req,res) =>{

});

routes.get('/home/list', ClientController.list);
routes.get('/home/searchID/:id', ClientController.searchId)
routes.post('/home/registra', ClientController.create);
routes.put('/home/update/:id', ClientController.update);
routes.delete('/home/deleta/:id', ClientController.delete);




module.exports = routes;