const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const ClientSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true,
    },
    apelido: {
        type: String,
    },
    telefone: {
        type: String,
        required:true,
    },
    endereco: {
        type: String,
        required: true,
    },
    login: {
        type: String,
        required: true,
    },
    senha: {
        type: String,
        required: true,
    },
    superUsuario: {
        type: Boolean,
        required: true,
    }
});

ClientSchema.plugin(mongoosePaginate);
mongoose.model('Client', ClientSchema);